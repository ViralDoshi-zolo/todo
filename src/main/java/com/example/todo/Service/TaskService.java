package com.example.todo.Service;

import com.example.todo.Repository.TaskRepository;
import com.example.todo.dto.ConversionHelperMapper;
import com.example.todo.dto.TaskRequestDTO;
import com.example.todo.dto.TaskResponseDTO;
import com.example.todo.Model.Task;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ConversionHelperMapper conversionHelper;

    public String welcome(){
        return "Welcome to my ToDo list of Tasks";
    }

    public List<TaskResponseDTO> printAllTasks(){

        List<Task> taskList =  taskRepository.findAll();
        List<TaskResponseDTO> taskResponseDTOList = new ArrayList<TaskResponseDTO>();
        for (Task t: taskList)
            taskResponseDTOList.add(conversionHelper.TaskModelToResponse(t));
        return taskResponseDTOList;
    }

    public void addNewTask(TaskRequestDTO taskRequestDTO) {
        Task task = conversionHelper.TaskRequestToModel(taskRequestDTO);
        taskRepository.save(task);
    }

    public String deleteTaskById(Long taskId) {
        boolean present = taskRepository.existsById(taskId);
        if (!present){
            throw new IllegalStateException("task with ID " + taskId + " does not exist");
        }
        else {
            taskRepository.deleteById(taskId);
            return "Deleted Successfully";
        }
    }

    @Transactional
    public void updateTask(Long taskId, String name, boolean urgent) {
        Task task = taskRepository.findById(taskId).orElseThrow(() -> new IllegalStateException("Task with ID " + taskId + " is not present"));
        task.setName(name);
        task.setUrgent(urgent);
    }

    public TaskResponseDTO printTask(Long taskId) {
        Task task = taskRepository.findById(taskId).orElseThrow(() -> new IllegalStateException("Task with ID " + taskId + " is not present"));
        TaskResponseDTO taskResponseDTO = conversionHelper.TaskModelToResponse(task);
        return taskResponseDTO;
    }
}
