package com.example.todo.Service;

import com.example.todo.Model.User;
import com.example.todo.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<User> printAllUsers(){
        return userRepository.findAll();
    }

    public void addNewUser(User user) {
        userRepository.save(user);
    }
}
