package com.example.todo.dto;

import com.example.todo.Model.Task;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class ConversionHelperMapper {

    public Task TaskRequestToModel(TaskRequestDTO taskRequestDTO){
        Task model = new Task();
        model.setName(taskRequestDTO.getName());
        model.setCategory(taskRequestDTO.getCategory());
        model.setUrgent(taskRequestDTO.isUrgent());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dueDate = LocalDateTime.parse(taskRequestDTO.getDueDate(), formatter);
        model.setDueDate(dueDate);

        return model;
    }

    public TaskResponseDTO TaskModelToResponse(Task model){
        TaskResponseDTO taskResponseDTO = new TaskResponseDTO();
        taskResponseDTO.setTaskId(model.getTaskId());
        taskResponseDTO.setName(model.getName());
        taskResponseDTO.setCategory(model.getCategory());
        taskResponseDTO.setDueDate(model.getDueDate());
        taskResponseDTO.setUrgent(model.isUrgent());

        Duration durationLeft = Duration.between(LocalDateTime.now(), model.getDueDate());
        Long daysLeft = durationLeft.toDays();
        Long hoursLeft = durationLeft.toHours() - durationLeft.toDays()*24;
        Long minutesLeft = durationLeft.toMinutes()- durationLeft.toHours()*60;

        taskResponseDTO.setDaysLeft(daysLeft);
        taskResponseDTO.setHoursLeft(hoursLeft);
        taskResponseDTO.setMinutesLeft(minutesLeft);

        return taskResponseDTO;
    }
}
