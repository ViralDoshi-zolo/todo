package com.example.todo.dto;


import com.example.todo.Model.Task;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class TaskRequestDTO {

    private String name;
    private Task.Category category;
    private String dueDate;
    private boolean urgent;
    public enum Category {
        WORK, ACADEMIC, PERSONAL
    }
}
