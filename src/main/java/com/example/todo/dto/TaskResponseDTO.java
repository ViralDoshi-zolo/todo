package com.example.todo.dto;

import com.example.todo.Model.Task;
import lombok.*;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class TaskResponseDTO {
    private Long taskId;
    private String name;
    private Task.Category category;
    private LocalDateTime dueDate;
    private boolean urgent;
    private Long daysLeft;
    private Long hoursLeft;
    private Long minutesLeft;
    public enum Category {
        WORK, ACADEMIC, PERSONAL
    }
}
