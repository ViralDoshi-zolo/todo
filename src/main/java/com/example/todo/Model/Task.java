package com.example.todo.Model;

import jakarta.persistence.*;
import lombok.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "task")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long taskId;
    private String name;
    private Category category;
    private LocalDateTime dueDate;
    private boolean urgent;

    public enum Category {
        WORK, ACADEMIC, PERSONAL
    }

    public Task(String name, Category category, LocalDateTime dueDate, boolean urgent) {
        this.name = name;
        this.category = category;
        this.dueDate = dueDate;
        this.urgent = urgent;
    }
}
