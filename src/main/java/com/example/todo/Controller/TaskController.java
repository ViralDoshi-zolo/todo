package com.example.todo.Controller;

import com.example.todo.dto.TaskRequestDTO;
import com.example.todo.dto.TaskResponseDTO;
import com.example.todo.Service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class TaskController {

    @Autowired
    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping
    public String welcome(){
        return taskService.welcome();
    }

    @GetMapping("/tasks")
    public List<TaskResponseDTO> printAllTasks(){
        return taskService.printAllTasks();
    }

    @GetMapping("/tasks/{taskId}")
    public TaskResponseDTO printTask(@PathVariable("taskId") Long taskId){
        return taskService.printTask(taskId);
    }

    @PostMapping("/tasks")
    public void addNewTask(@RequestBody TaskRequestDTO taskRequestDTO){
        taskService.addNewTask(taskRequestDTO);
    }

    @DeleteMapping("/tasks/{taskId}")
    public String deleteTaskById(@PathVariable("taskId") Long taskId){

        return taskService.deleteTaskById(taskId);
    }

    @PutMapping("tasks/{taskId}")
    public void updateTaskById(
            @PathVariable("taskId") Long taskId,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) boolean urgent){
        taskService.updateTask(taskId, name, urgent);
    }

}
