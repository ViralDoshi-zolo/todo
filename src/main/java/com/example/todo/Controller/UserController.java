package com.example.todo.Controller;

import com.example.todo.Model.User;
import com.example.todo.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("users")
    public List<User> printAllUsers(){
        return userService.printAllUsers();
    }

    @PostMapping("users")
    public void addNewUser(@RequestBody User user){
        userService.addNewUser(user);

    }

}
